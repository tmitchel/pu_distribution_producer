from PU_calc.PUAnalyzer.PUAnalyzer_cfi import *
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing

options = VarParsing('analysis')
options.register('filename', '',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 'path to input file'
                 )
options.register('output', '',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 'name for output file'
                 )
options.register('dataset', '',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 'dataset name for histogram'
                 )
options.setDefault('maxEvents', -1)
options.parseArguments()

process = cms.Process("PUAnalyzer")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 10000

process.maxEvents = cms.untracked.PSet(input=cms.untracked.int32(-1))

# example: '/store/mc/RunIIFall17MiniAODv2/DY1JetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/MINIAODSIM/PU2017_12Apr2018_v3_94X_mc2017_realistic_v14_ext1-v2/100000/3491E123-F1B8-E811-AFD7-E0071B7A7860.root'
process.source = cms.Source("PoolSource", fileNames=cms.untracked.vstring(options.filename))

process.pua = PUAnalyzer.clone(
    histName = cms.string(options.dataset)
)

process.TFileService = cms.Service("TFileService",
    fileName=cms.string(options.output)
                                   )
process.p = cms.Path(process.pua)
