import FWCore.ParameterSet.Config as cms

PUAnalyzer = cms.EDAnalyzer(
    "PUAnalyzer",
    puInfoSrc = cms.InputTag("slimmedAddPileupInfo"),
    histName = cms.string('')
)
