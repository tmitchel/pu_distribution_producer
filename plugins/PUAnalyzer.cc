// -*- C++ -*-
//
// Package:    PU-calc/PUAnalyzer
// Class:      PUAnalyzer
//
/**\class PUAnalyzer PUAnalyzer.cc PU-calc/PUAnalyzer/plugins/PUAnalyzer.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Tyler Mitchell
//         Created:  Mon, 29 Jul 2019 17:05:37 GMT
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/TrackReco/interface/TrackFwd.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"

#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TH1.h"

//
// class declaration
//

using reco::TrackCollection;

class PUAnalyzer : public edm::one::EDAnalyzer<> {
   public:
    explicit PUAnalyzer(const edm::ParameterSet&);
    ~PUAnalyzer();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

   private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;

    // ----------member data ---------------------------
    edm::EDGetTokenT<std::vector<PileupSummaryInfo> > puInfoSrcToken_;
    std::string histName;
    TH1D *puDist;
};

//
// constructors and destructor
//
PUAnalyzer::PUAnalyzer(const edm::ParameterSet& pset)
    : puInfoSrcToken_(consumes<std::vector<PileupSummaryInfo> >(pset.getParameter<edm::InputTag>("puInfoSrc"))),
    histName(pset.getParameter<std::string>("histName"))
{
    edm::Service<TFileService> fs;
    puDist = fs->make<TH1D>("puDist", "PU Distribution", 200, 0, 200);
}

PUAnalyzer::~PUAnalyzer() {
}

// ------------ method called for each event  ------------
void PUAnalyzer::analyze(const edm::Event& evt, const edm::EventSetup& iSetup) {
    edm::Handle<std::vector<PileupSummaryInfo> > puInfo;
    evt.getByToken(puInfoSrcToken_, puInfo);
    if (puInfo.isValid()) {
        puDist->Fill(puInfo->at(0).getTrueNumInteractions(), 1);
    }
    puDist->SetName(histName.c_str());
}

// ------------ method called once each job just before starting event loop  ------------
void PUAnalyzer::beginJob() {}

// ------------ method called once each job just after ending the event loop  ------------
void PUAnalyzer::endJob() {}

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void PUAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    // edm::ParameterSetDescription desc;
    // desc.addUntracked<edm::InputTag>("puInfoSrc");
    // descriptions.addDefault(desc);
}

// define this as a plug-in
DEFINE_FWK_MODULE(PUAnalyzer);
